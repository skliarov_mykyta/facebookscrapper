from fbscrapper import FacebookScrapper

with  FacebookScrapper() as fb_session:
    prof_id = '100001630473283'
    profile_html = fb_session.get_profile_html(profile_id=prof_id)
    friends_html = fb_session.get_friends_html(profile_id=prof_id)
    about_html = fb_session.get_about_html(profile_id=prof_id)
    FacebookScrapper.parse_profile_data(about_html)
    FacebookScrapper.parse_friends(friends_html)

    # Save recieved html to files to check

    # with open('profile.html', 'wb') as f:
    #     f.write(profile_html)
    # with open('friends.html', 'wb') as f:
    #     f.write(friends_html)
    # with open('about.html', 'wb') as f:
    #     f.write(about_html)

# Perform parsing from files

# with open('about.html', 'r') as about:
#     about_html = about.read()
#     FacebookScrapper.parse_profile_data(about_html)

# with open('friends.html', 'r') as friends:
#     friends_html = friends.read()
#     FacebookScrapper.parse_friends(friends_html)