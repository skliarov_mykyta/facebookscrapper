import json
import sys
from config import URLS, CREDENTIALS, SELECTORS
from bs4 import BeautifulSoup

if sys.version_info >= (3, 0):
    import mechanicalsoup
    browser = mechanicalsoup.StatefulBrowser()
else:
    import mechanize
    browser = mechanize.Browser()


class FacebookScrapper:
    def __init__(self, email: str = CREDENTIALS['email'], password: str = CREDENTIALS['pass']):
        self._credentials = {'email': email, 'pass': password}
        self._browser = browser
        self._logged_in = False

    def __enter__(self):
        self.login()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._logged_in:
            self.logout()

    def login(self, login_url: str = URLS['LOGIN_URL']):
        self._browser.open(login_url)
        login_form = self._browser.select_form()
        login_form.set_input(self._credentials)
        try:
            res = self._browser.submit_selected()
            res.raise_for_status()
            html_soup = BeautifulSoup(res.content, 'lxml')
            if html_soup.select_one(SELECTORS['login_check']) is None:
                raise Exception('Authorization failed')
        except:
            raise
        self._logged_in = True

    def logout(self, logout_url: str = URLS['LOGOUT_URL']):
        self._browser.open(logout_url)
        self._logged_in = False

    def get_profile_html(self, profile_id: str) -> str:
        if self._logged_in:
            res = self._browser.open(URLS['MOBLIE_PROFILE_ID_BASED_URL'].format(profile_id=profile_id))
            res.raise_for_status()
            return res.content
        else:
            raise Exception('Authorization required')

    def get_friends_html(self, profile_id: str) -> str:
        if self._logged_in:
            res = self._browser.open(URLS['MOBLIE_PROFILE_ID_BASED_FRIENDS_URL'].format(profile_id=profile_id))
            res.raise_for_status()
            return res.content
        else:
            raise Exception('Authorization required')

    def get_about_html(self, profile_id: str) -> str:
        if self._logged_in:
            res = self._browser.open(URLS['MOBLIE_PROFILE_ID_BASED_ABOUT_URL'].format(profile_id=profile_id))
            res.raise_for_status()
            return res.content
        else:
            raise Exception('Authorization required')

    @staticmethod
    def parse_profile_data(about_html) -> dict:
        html_soup = BeautifulSoup(about_html, 'lxml')
        profile_data = {}

        profile_data['work'] = []
        work_container = html_soup.select(SELECTORS['profile_work'])
        for span in work_container:
            if span.select_one('a'):
                profile_data['work'].append(span.text + ' ' + span.select_one('a')['href'])
            profile_data['work'].append(span.text)

        profile_data['education'] = []
        edu_container = html_soup.select(SELECTORS['profile_edu'])
        for span in edu_container:
            if span.text:
                profile_data['education'].append(span.text)

        profile_data['living'] = []
        living_container = html_soup.select(SELECTORS['profile_living'])[1:]
        for span in living_container:
            if span.text:
                profile_data['living'].append(span.text)

        profile_data['basic-info'] = []
        basic_info_container = html_soup.select(SELECTORS['profile_basic'])[1:]
        for span in basic_info_container:
            if span.text:
                profile_data['basic-info'].append(span.text)

        with open('profile_data.json', 'w') as profile_data_json:
            json.dump(profile_data, profile_data_json, indent=4, ensure_ascii=False)

        return profile_data

    @staticmethod
    def parse_friends(friends_html) -> list:
        friends = []
        html_soup = BeautifulSoup(friends_html, 'lxml')
        friends_container = html_soup.select(SELECTORS['friends_base'])[2:]
        for table in friends_container:
            if(table.select_one(SELECTORS['friends_img']) is not None and
               table.select_one(SELECTORS['friends_info']) is not None):
                friend_info = {}
                friend_info['img'] = table.select_one(SELECTORS['friends_img'])['src']
                friend_info['link'] = table.select_one(SELECTORS['friends_info'])['href']
                friend_info['name'] = table.select_one(SELECTORS['friends_info']).text
                friends.append(friend_info)

        with open('friends.json', 'w') as friend_json:
            json.dump(friends, friend_json, indent=4, ensure_ascii=False)

        return friends
