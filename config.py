MOBILE_BASE_URL = 'https://m.facebook.com'
BASE_URL = 'https://www.facebook.com'
URLS = {
    'LOGIN_URL': BASE_URL + '/login.php',
    'LOGOUT_URL': BASE_URL + '/login/device-based/regular/logout/?button_name=logout&button_location=settings',
    'PROFILE_ID_BASED_URL': BASE_URL + '/{profile_id}',
    'PROFILE_ID_BASED_FRIENDS_URL': BASE_URL + '/{profile_id}/friends',
    'PROFILE_ID_BASED_ABOUT_URL': BASE_URL + '/{profile_id}/about',
    'MOBLIE_PROFILE_ID_BASED_URL': MOBILE_BASE_URL + '/profile.php?id={profile_id}',
    'MOBLIE_PROFILE_ID_BASED_FRIENDS_URL': MOBILE_BASE_URL + '/profile.php?v=friends&id={profile_id}',
    'MOBLIE_PROFILE_ID_BASED_ABOUT_URL': MOBILE_BASE_URL + '/profile.php?v=info&id={profile_id}',
}
CREDENTIALS = {
    'email': '',
    'pass': ''
}
USER_AGENTS = ('Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.517.41 Safari/534.7', )
SELECTORS = {
    'login_check': 'a[name="mercurymessages"]',
    'profile_work': 'div[id="work"] span',
    'profile_edu': 'div[id="education"] span',
    'profile_living': 'div[id="living"] td',
    'profile_basic': 'div[id="basic-info"] td',
    'friends_base': 'tr',
    'friends_img': 'td img',
    'friends_info': 'td a',
}